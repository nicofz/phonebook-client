import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { Contact }  from '../../interfaces/contact'

@Component({
  selector: 'app-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss'],
})
export class FormModalComponent implements OnInit {
  title: string
  contactForm: Contact

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {}

  cancel() {
    return this.modalCtrl.dismiss(null, 'cancel');
  }

  confirm() {
    return this.modalCtrl.dismiss(this.contactForm, 'confirm');
  }
}
