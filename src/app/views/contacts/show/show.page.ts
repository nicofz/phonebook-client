import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactService } from 'src/app/service/contact.service';
import { ModalController, AlertController } from '@ionic/angular'
import { FormModalComponent } from '../../../components/form-modal/form-modal.component'

import { Contact }  from '../../../interfaces/contact'

@Component({
  selector: 'app-show',
  templateUrl: './show.page.html',
  styleUrls: ['./show.page.scss'],
})
export class ShowPage implements OnInit {
  contactId: string;
  contact: Contact
  error = ''
  loading = false

  constructor(
    private activatedRoute: ActivatedRoute,
    private contactService: ContactService,
    private modalCtrl: ModalController,
    private router: Router,
    private alertCtrl: AlertController
    ) { }

  ngOnInit() {
    this.contactId = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ionViewWillEnter() {
    this.loading = true
    this.getContact()
  }

  ionViewDidLeave() {
    this.error = ''
  }

  async openModal(contact) {
    const modal = await this.modalCtrl.create({
      component: FormModalComponent,
      componentProps: {
        contactForm: {...contact},
        title: 'Edit'
      }
    });
    modal.present();

    const {data, role} = await modal.onWillDismiss()

    if (role === 'confirm') {
      this.error = ''
      this.updateContact(data)
      this.loading = true
    }
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Are you sure!',
      cssClass: 'my-custom-class',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {  }
        },
        {
          text: 'Yes',
          role: 'confirm',
          handler: () => {
            this.error = ''
            this.loading = true
            this.deleteContact(this.contact.id)
          }
        }
      ]
    });

    await alert.present();

    // const { role } = await alert.onDidDismiss();
  }

  getContact() {
    this.contactService.get(this.contactId).subscribe((res: Contact) => {
      this.contact = res
      this.loading = false
    }, err => {
      this.error = err.error.message
      this.loading = false
    })
  }

  updateContact(contact: Contact) {
    this.contactService.update(contact).subscribe((res: Contact) => {
      this.contact = res
      this.loading = false
    }, err => {
      this.error = err.error.message
      this.loading = false
    })
  }

  deleteContact(id: number) {
    this.contactService.delete(id).subscribe((res: Contact) => {
      this.loading = false
      this.router.navigate(['/contacts'])
    }, err => {
      this.error = err.error.message
      this.loading = false
    })
  }

  doRefresh(event) {
    this.contactService.get(this.contactId).subscribe((res: Contact) => {
      this.contact = res
      event.target.complete();
    }, err => {
      this.error = err.error.message
      event.target.complete();
    })
  }
}
