import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  url = 'http://192.168.1.9:3000'
  // url = 'http://localhost:3000'
  headers = new HttpHeaders({
    'accept': 'application/json'
  })
  constructor(private httpClient: HttpClient) { }

  get(endpoint: string) {
    return this.httpClient.get(this.url + endpoint, { headers: this.headers });
  }

  post(endpoint: string, body?: any) {
    return this.httpClient.post(this.url + endpoint, body, { headers: this.headers });
  }

  put(endpoint: string, body?: any) {
    return this.httpClient.put(this.url + endpoint, body);
  }

  patch(endpoint: string, body?: any) {
    return this.httpClient.put(this.url + endpoint, body, { headers: this.headers });
  }

  delete(endpoint: string) {
    return this.httpClient.delete(this.url + endpoint, { headers: this.headers });
  }
}
