import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

import { Contact }  from '../interfaces/contact'

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private api: ApiService) { }

  all() {
    return this.api.get('/contacts')
  }

  get(id: string) {
    return this.api.get(`/contacts/${id}`)
  }

  update(contact: Contact) {
    return this.api.patch(`/contacts/${contact.id}`, contact)
  }

  add(contact: Contact) {
    return this.api.post(`/contacts`, contact)
  }

  delete(id: number) {
    return this.api.delete(`/contacts/${id}`)
  }
}
