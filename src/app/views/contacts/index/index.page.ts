import { Component, OnInit } from '@angular/core';
import { ContactService } from 'src/app/service/contact.service';
import { ModalController } from '@ionic/angular'
import { FormModalComponent } from '../../../components/form-modal/form-modal.component'

import { Contact }  from '../../../interfaces/contact'

@Component({
  selector: 'app-index',
  templateUrl: './index.page.html',
  styleUrls: ['./index.page.scss'],
})
export class IndexPage implements OnInit {
  contacts: Contact[] = []
  error = ''
  loading = false

  constructor(
    private contactService: ContactService,
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
    this.loading = true
  }

  ionViewWillEnter() {
    this.error = ''
    this.loading = true
    this.getContacts()
  }

  ionViewDidLeave() {
    this.error = ''
  }

  async openModal() {
    const modal = await this.modalCtrl.create({
      component: FormModalComponent,
      componentProps: {
        contactForm: {
          name: '',
          email: '',
          phone: ''
        },
        title: 'Add'
      }
    });
    modal.present();

    const {data, role} = await modal.onWillDismiss()

    if (role === 'confirm') {
      this.error = ''
      this.loading = true
      this.addContact(data)
    }
  }

  getContacts() {
    this.error = ''
    this.contactService.all().subscribe((res: Contact[]) => {
      this.contacts = res
      this.loading = false
    },(err) => {
      console.log(err);
      this.error = err.message
      this.loading = false
    })
  }

  addContact(contact) {
    this.error = ''
    this.contactService.add(contact).subscribe(
      (res: Contact) => {
        this.contacts.unshift(res)
        this.loading = false
      },
      (err) => {
        this.error = err.error.message
        this.loading = false
      }
    )
  }

  doRefresh(event?) {
    this.error = ''
    if (!event) {
      this.loading = true
      this.getContacts()
      return
    }

    this.contactService.all().subscribe((res: Contact[]) => {
      this.contacts = res
      event.target.complete();
    },(err) => {
      this.error = err.message
      event.target.complete();
    })
  }
}
